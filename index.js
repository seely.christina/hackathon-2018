var express = require('express');

// Instantiates Express and assigns our app variable to it
var app = express();

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

require('./src/routes') (app, {});

// Port we want to listen to
const PORT=4390;

// Lets start our server
app.listen(PORT, function () {
    //Callback triggered when server is successfully listening. Hurray!
    console.log("Poke app listening on port " + PORT);
});

let state = {
    currentChannel: {},
    currentPokemon: {},
}

module.exports.state = state;