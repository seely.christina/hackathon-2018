var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/pokemon";

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("pokedb");
  dbo.createCollection("pokedex", function(err, res) {
    if (err) throw err;
    console.log("Collection created!");
    db.close();
  });
});