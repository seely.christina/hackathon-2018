var db = require('../config/db');
var user = require('../services/user');

module.exports.giftItem = function (giver, receiver, item) {
    return new Promise(function (resolve, reject) {
        const get_user_gift_item_sql = `SELECT * FROM user_gift_item WHERE userId='${giver}' AND itemId=${item}`;

        db.all(get_user_gift_item_sql, [], (err, items) => {
            if (err) {
                throw err;
            }

            if( items.length === 1 && items[0].quantity > 0){
                resolve(items[0].quantity);
            } else {
                reject("You don't have any to give!");
            }
        });
    }).then(function(quantity) {
        const get_user_item_sql = `SELECT * FROM user_item WHERE userID='${receiver}' AND itemId=${item}`;

        return new Promise(function(resolve,reject){
            db.all(get_user_item_sql, [], (err,items) => {
                if(err) {
                    throw err;
                }
    
                if( items.length === 1){
                    db.run(`UPDATE user_item SET quantity=${items[0].Quantity + 1} WHERE itemId=${item} and userId='${receiver}'`,[], (err)=>{
                        if(err){
                            throw err;
                        }
                        resolve(quantity);
                    });
                } else {
                    db.run('INSERT INTO user_item(userid, itemid, quantity) VALUES(?, ?, ?)', [receiver, item, 1], (err) => {
                        if (err) {
                            throw err;
                        }
                        resolve(quantity);
                    });
                }
            });
        });
    }).then((quantity)=>{
        return new Promise(function(resolve,reject){
            db.run(`UPDATE user_gift_item SET quantity=${quantity - 1} WHERE itemId=${item} and userId='${giver}'`,[], (err)=>{
                if(err){
                    throw err;
                }
                resolve();
            });
        })
    });
};

module.exports.usePokeball = function(userName) {
    return new Promise(function(resolve, reject) {
        db.run('UPDATE user_item SET quantity = quantity - 1 WHERE userid = ? and itemid = ?', [userName, 4], (err) => {
            if (!err) {
                console.log("removed one pokeball for", userName);
            } else {
                console.log("err", err);
            }
            return resolve()
        });
    });
};

module.exports.getPokeballs = function(userName){
    return new Promise(function(resolve, reject) {
        db.get('SELECT * FROM user_item WHERE userid = ? and itemid = ?', [userName, 4], (err, rows) => {
            if (err) {
                throw err;
            }
            return resolve(rows)
        });
    });
};

module.exports.getGiftPokeballs = function(userName) {
    return new Promise(function(resolve) {
        db.get(`SELECT * FROM user_gift_item WHERE userId=? and itemid = ?`, [userName, 4], (err, rows) => {
            if (err) {
                throw err;
            }
            return resolve(rows);
        });
    });
};
