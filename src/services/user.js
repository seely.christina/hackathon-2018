var db = require('../config/db');
var createUser;

module.exports.getUser = function(userName){
    return new Promise(function(resolve, reject) {
        db.get('SELECT * FROM user WHERE id = ?', [userName], (err, row) => {
            if (err) {
                throw err;
            }

            if (!row) {
                console.log("User does not exist. Create user!", userName);
                return createUser(userName)
                    .then((user) => {
                        resolve(user);
                    });
            } else {
                return resolve(row)
            }
        });
    });
};

module.exports.userExists = function(userName){
    return new Promise(function(resolve, reject) {
        db.get('SELECT * FROM user WHERE id = ?', [userName], (err, row) => {
            if (err) {
                throw err;
            }

            resolve(!!row);
        });
    });
};

createUser = function (userName) {
    return new Promise(function (resolve, reject) {
        db.run('INSERT INTO user(id) VALUES(?)', [userName], (err) => {
            console.log("created user", userName);
            db.run('INSERT INTO user_item(userid, itemid, quantity) VALUES(?, ?, ?)', [userName, 4, 10], (err) => {
                console.log("created user_items", userName);
                db.run('INSERT INTO user_gift_item(userid, itemid, quantity) VALUES(?, ?, ?)', [userName, 4, 10], (err) => {
                    console.log("created user_gift_items", userName);
                    resolve(userName);
                });
            });
        });
    });
};