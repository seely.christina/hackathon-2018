var db = require('../config/db');

let pokedex = [];
const sql = `SELECT * FROM pokemon`;

db.all(sql, [], (err, rows) => {
  if (err) {
    throw err;
  }

  pokedex = rows;
});

module.exports.getPokedex = function(userId){
  return new Promise(function(resolve, reject){
    const sql = `SELECT * FROM user_pokedex JOIN pokemon ON user_pokedex.pokemonId=pokemon.id WHERE userid='${userId}' `;

    db.all(sql, [], (err, rows) => {
      if (err) {
        throw err;
      }
    
      const pokemon = rows.map((pokemon)=>`:pokemon_${pokemon.id}: ${pokemon.identifier}`).join("\n");

      resolve({
        count: rows.length,
        pokemon
      });
    });
  });
};

module.exports.getPokedexCount = function () {
  return pokedex.length;
};

module.exports.addPokemon = function (userId, pokemonId) {
    console.log("ids", typeof userId, userId, typeof pokemonId, pokemonId);
    return new Promise(function (resolve, reject) {
        db.get('SELECT * FROM user_pokedex where userid = ? and pokemonid = ?', [userId, pokemonId], (err, rows) => {
            console.log("res", rows);
           if (!rows) {
               db.run('INSERT INTO user_pokedex(userid, pokemonid) VALUES(?, ?)', [userId, pokemonId], (err) => {
                   console.log("added pokemon", pokemonId, "to", userId, "'s pokedex");
                   resolve();
               });
           } else {
               resolve();
           }
        });
    });
};
