var db = require('../config/db');
var pokemonGen = require('../services/pokemon_gen');
var pokemonAlert = require('../webhooks/pokemon_alert');
var channel = require('../services/channel');
var index = require('../../index.js');

let pokemon_species = [];
const pokemonTimeLimit = 30000;
module.exports.pokemonTimeLimit = pokemonTimeLimit;
const sql = `SELECT id, identifier, picture_name FROM pokemon WHERE id <= 151`;
db.all(sql, [], (err, rows) => {
    if (err) {
        throw err;
    }
    pokemon_species = rows;
});

module.exports.getPokemon = function(){
    var pokemon = pokemon_species[Math.floor(Math.random() * 150)];
    return pokemon;
};

setInterval(function() {
    let pokemon = index.state.currentPokemon;
    let messageObj;
    if (!pokemon || Object.keys(pokemon).length === 0) {
        pokemon = pokemonGen.getPokemon();
        pokemon.timeSpawned = Date.now();
        pokemon.available = true;
        index.state.currentChannel = channel.getRandomChannel();
        messageObj = pokemonAlert.getPokemonMessageObject(pokemon);
    } else if (Date.now() - pokemon.timeSpawned > pokemonTimeLimit) {  // If a pokemon has spawned for 5 minutes, remove it
        if (pokemon.available) {
            messageObj = pokemonAlert.getPokemonRunAwayMessageObject(pokemon);
        } else {
            index.state.currentPokemon = null;
        }
        pokemon = null;
    }
    // messageObj is only defined if the a pokemon has left or spawned
    if (messageObj) {
        index.state.currentPokemon = pokemon;
        pokemonAlert.sendPokemonAlert(messageObj, index.state.currentChannel);
    }
}, 1000)
