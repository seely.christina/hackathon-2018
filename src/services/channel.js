var db = require('../config/db');

let allChannels = [];
const sql = `SELECT * FROM channel`;
db.all(sql, [], (err, rows) => {
    if (err) {
        throw err;
    }
    allChannels = rows;
});

module.exports.getRandomChannel = function(){
    var channel = allChannels[Math.floor(Math.random() * allChannels.length)];
    console.log(channel);
    return channel;
};