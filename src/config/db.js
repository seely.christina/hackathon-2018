const sqlite3 = require('sqlite3').verbose();

let db = new sqlite3.Database('./dist/veekun-pokedex.sqlite', (err) => {
  if (err) {
    console.error(err.message);
  }
  console.log('Connected to the pokedex.');
});

module.exports = db;
// let sql = `SELECT * FROM pokemon`;

// db.all(sql, [], (err, rows) => {
//   if (err) {
//     throw err;
//   }
//   rows.forEach((row) => {
//     console.log(row);
//   });
// });


// close the database connection
// db.close();