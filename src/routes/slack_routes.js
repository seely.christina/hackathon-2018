var request = require('request');
var pokedex = require('../services/pokedex');
var user = require('../services/user');
var user_item = require('../services/user_item');
var {pokemonGen, pokemonTimeLimit} = require('../services/pokemon_gen');
var index = require('../../index.js');
var pokemonAlert = require('../webhooks/pokemon_alert');
var channel = require('../services/channel');
const axios = require('axios');

// Store our app's ID and Secret
// For an actual app, these should be stored securely in environment variables.
var clientId = '436037406706.461607976822';
var clientSecret = '80d366b4892d189decfa3d1bf3b3f7d3';
var bot_token = 'Bearer xoxb-436037406706-462207738582-hCJx4jybKd2LVi5uJfiSuDIM';

module.exports = function (app, db) {

    // This route handles get request to a /oauth endpoint. We'll use this endpoint for handling the logic of the Slack oAuth process behind our app.
    app.get('/oauth', function(req, res) {
        // When a user authorizes an app, a code query parameter is passed on the oAuth endpoint. If that code is not there, we respond with an error message
        if (!req.query.code) {
            res.status(500);
            res.send({"Error": "Looks like we're not getting code."});
            console.log("Looks like we're not getting code.");
        } else {
            // If it's there...

            // We'll do a GET call to Slack's `oauth.access` endpoint, passing our app's client ID, client secret, and the code we just got as query parameters.
            request({
                url: 'https://slack.com/api/oauth.access', //URL to hit
                qs: {code: req.query.code, client_id: clientId, client_secret: clientSecret}, //Query string data
                method: 'GET', //Specify the method

            }, function (error, response, body) {
                if (error) {
                    console.log(error);
                } else {
                    res.json(body);

                }
            })
        }
    });

    app.post('/throwpokeball', function (req, res) {
        user.getUser(req.body.user_name)
            .then((user) => {
                // Check that user has pokeballs
                user_item.getPokeballs(req.body.user_name)
                    .then((pokeballs) => {
                        if (index.state.currentChannel.id === req.body.channel_id && pokeballs.Quantity > 0 && index.state.currentPokemon.available) {
                            var text;
                            var result;
                            var noPokemonMessage = `@${req.body.user_name} throws :pokeball: at the ground. They hit it.`;

                            var pokemon = index.state.currentPokemon.identifier;
                            pokemon = pokemon.charAt(0).toUpperCase() + pokemon.slice(1);
                            var pokemonId = index.state.currentPokemon.id;

                            if (!pokemon) {
                                text = noPokemonMessage;
                            } else {
                                var missedMessage = 'They missed.';
                                var pokemonFledMessage = `${pokemon} ran away.`;
                                var caughtPokemonMessage = `${pokemon} was caught!`;
                                var rand = Math.random();

                                if (rand < .8) {
                                    result = caughtPokemonMessage;
                                    pokedex.addPokemon(req.body.user_name, pokemonId);

                                    // Set pokemon as unavailable
                                    index.state.currentPokemon.available = false;
                                } else if (rand < .95) {
                                    result = missedMessage;
                                } else {
                                    result = pokemonFledMessage;
                                    // Set pokemon as unavailable
                                    index.state.currentPokemon.available = false;
                                }
                                text = `@${req.body.user_name} throws :pokeball: at ${pokemon}. ${result}`;
                            }

                            // decrement pokeball count
                            user_item.usePokeball(req.body.user_name);

                            res.send({
                                "parse": "full",
                                "link_names": 1,
                                "text": text,
                                "response_type": "in_channel",
                            });
                        } else if (pokeballs.Quantity > 0) {
                            // decrement pokeball count
                            user_item.usePokeball(req.body.user_name);
                            res.send({
                                "parse": "full",
                                "link_names": 1,
                                "text": `@${req.body.user_name} throws :pokeball: at the ground. They hit it.`,
                                "response_type": "in_channel",
                            });
                        } else {
                            res.send({
                                "parse": "full",
                                "link_names": 1,
                                "text": `@${req.body.user_name} has no :pokeball: left and throws their arms up in frustration.`,
                                "response_type": "in_channel",
                            });
                        }
                    })

            });
    });

    app.post('/bag', function(req, res) {
        console.log("get user bag for", req.body.user_name);
        user.getUser(req.body.user_name)
            .then((user) => {
                console.log("got user", user);
                var pokeballPromise = user_item.getPokeballs(req.body.user_name);
                var giftPromise = user_item.getGiftPokeballs(req.body.user_name);
                Promise.all([pokeballPromise, giftPromise]).then((responses) => {
                    var pokeballs = responses[0];
                    var gifts = responses[1];
                    res.send({
                        "text": `*Your bag*: ${pokeballs.Quantity} :pokeball:\n*Gift bag*: ${gifts.quantity} :pokeball:`,
                    });
                });
            });
    });

    app.post('/pokedex', function(req, res) {
        pokedex.getPokedex(req.body.user_name).then(function(pokedex){
            res.send({
                "text": `You have *${pokedex.count} pokemon* in your pokedex! :pokedex:`,
                "attachments": [
                    {
                        "text": pokedex.pokemon
                    }
                ]
            });
        });
    });

    app.post('/pokemontoggle', function (req, res) {
        let pokemon = index.state.currentPokemon;
        let messageObj;
        if (!pokemon || Object.keys(pokemon).length === 0) {
            pokemon = pokemonGen.getPokemon();
            pokemon.timeSpawned = Date.now();
            index.state.currentChannel = channel.getRandomChannel();
            messageObj = pokemonAlert.getPokemonMessageObject(pokemon.identifier);
        } else if (Date.now() - pokemon.timeSpawned > pokemonTimeLimit) {  // If a pokemon has spawned for 5 minutes, remove it
            messageObj = pokemonAlert.getPokemonRunAwayMessageObject(pokemon.identifier);
            pokemon = null;
        }
        state.currentPokemon = pokemon;
        pokemonAlert.sendPokemonAlert(messageObj, state.currentChannel);
        res.send(messageObj);
    });

    app.get('/currentPokemon', function(req, res) {
        var pokemon = index.state.currentPokemon;
        if (!pokemon || Object.keys(pokemon).length > 0) {
            res.send(pokemon.identifier);
        } else {
            res.send('No pokemon selected');
        }
    });

    app.post('/giftitem', function(req,res) {
        const channel_id = req.body.channel_id;
        let params = req.body.text.split(" ");
        const receiver_id = params[0].substr(1); 
        let item_id;

        switch(params[1]){
            case ':pokeball:' : item_id = 4; break;
        }

        user.userExists(receiver_id).then((exists)=>{
            if(req.body.user_name === receiver_id){
                res.send("That's no way to make friends!")
            } else if(item_id && exists){
                user_item.giftItem(req.body.user_name,receiver_id,item_id).then(()=>{
                    axios({
                        "url" : 'https://slack.com/api/chat.postMessage',
                        "method" : 'post',
                        "data" : {
                            "parse": "full",
                            "link_names": 1,
                            "channel" : channel_id,
                            "text" : `@${req.body.user_name} gave @${receiver_id} a :pokeball:.  They must have done something awesome!`
                        },
                        "headers" : {
                            "Authorization" : bot_token,
                            "Content-Type" : "application/json"
                        }
                    }).then((response)=>{
                        console.log(response);
                        res.send("Thanks for sharing!");
                    });
                }).catch((err)=>{
                    res.send(`You don't have any ${params[1]} to share.  Wait till tomorrow!`)
                });
            } else {
                res.send("We couldn't find the user or item you were trying to send.  Sorry!");
            }
        });
    });

    app.post('/receivemessage', function(req,res) {
        res.send({
            "challenge": req.body.challenge
        })
    });

};
