var slackRoutes = require('./slack_routes');

module.exports = function (app, db) {
    slackRoutes(app, db);
};