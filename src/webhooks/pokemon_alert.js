const channel = require('../services/channel');
const pokemonGen = require('../services/pokemon_gen');
const axios = require('axios');

module.exports.sendPokemonAlert = function(messageObj, currChannel){
    // Ed's webhook: https://hooks.slack.com/services/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
    axios.post(currChannel.webhook, messageObj)
        .catch(error => {
            console.log(error);
        });
};

parsePokemonName = function(pokemon) {
    if (!pokemon.picture_name || Object.keys(pokemon.picture_name).length === 0) {
        return pokemon.identifier;
    } else {
        return pokemon.picture_name;
    }
}

module.exports.getPokemonMessageObject = function(pokemon) {
    if (!pokemon) {
        return;
    }
    var baseUrl = 'http://www.pokestadium.com/sprites/black-white/animated/{{POKEMON}}.gif';
    // var baseUrl = 'http://www.pokestadium.com/sprites/xy/{{POKEMON}}.gif';

    var pokemonName = parsePokemonName(pokemon);
    if (pokemonName) {
        baseUrl = baseUrl.replace(/{{POKEMON}}/, pokemonName);
    } else {
        return '';
    }
    const pokemon_capitalized = pokemonName.charAt(0).toUpperCase() + pokemonName.substr(1);

    return {
        "attachments": [
            {
                "title": "A wild " + pokemon_capitalized + " has appeared! :pokeball:",
                "image_url": baseUrl
            },
        ]
    };
}


module.exports.getPokemonRunAwayMessageObject = function(pokemon) {
    if (!pokemon) {
        return;
    }

    var pokemonName = parsePokemonName(pokemon);
    const pokemon_capitalized = pokemonName.charAt(0).toUpperCase() + pokemonName.substr(1);

    return {
        "text": "*" + pokemon_capitalized + " has wandered off...* :disappointed:",
    };
}